function validateform() {
  let returnValue = true;
  let fnameValue = true;
  let lnameValue = true;
  let emailValue = true;
  let passValue = true;
  let countryValue = true;
  let phoneValue = true;
  clearError();

  // for the firstname validation in form
  let nameRegex = /^[A-Za-z]{3,15}$/;
  let fname = document.forms["myform"]["ffname"].value;
  if (nameRegex.test(fname)) {
    fnameValue = true;
  } else {
    setError("fname", "Firstname field should contain atleast 3 character");
    fnameValue = false;
  }

  // for lastname validation in form
  nameRegex = /^[A-Za-z]{3,15}$/;
  let lname = document.forms["myform"]["flname"].value;
  if (nameRegex.test(lname)) {
    lnameValue = true;
  } else {
    setError("lname", "Lastname field should contain atleast 3 character");
    lnameValue = false;
  }

  // for the email verification in form
  let emailRegex = /^([A-Za-z\d]+)@([a-zA-Z\d]+)\.([a-zA-Z\d]+)$/;
  let email = document.forms["myform"]["femail"].value;
  if (emailRegex.test(email)) {
    emailValue = true;
  } else {
    setError(
      "email",
      "email must contain the format the include @, ., character set"
    );
    emailValue = false;
  }

  // for password validation in form
  let passwordRegex = /^[a-zA-Z\d]{1,10}$/;

  let password = document.forms["myform"]["fpassword"].value;
  if (passwordRegex.test(password)) {
    passValue = true;
  } else {
    setError(
      "password",
      "Password must not be longer than 10 characters and shouldn't contain the special character"
    );
    passValue = false;
  }

  // for phone validation in form
  let phoneRegex = /^[0-9]{10}$/;
  let phone = document.forms["myform"]["fphone"].value;
  if (phoneRegex.test(phone)) {
    phoneValue = true;
  } else {
    setError("phone", "phone number should contain 10 numbers");
    phoneValue = false;
  }

  // for country validation in form
  let countryRegex = /\s/;
  let country = document.forms["myform"]["country"].value;
  if (countryRegex.test(country)) {
    setError("select-country", "select the country field");
    countryValue = false;
  } else {
    countryValue = true;
  }

  if (
    fnameValue &&
    lnameValue &&
    emailValue &&
    passValue &&
    phoneValue &&
    countryValue
  ) {
    returnValue = true;
    alert("successsfully validated the form");
  } else {
    returnValue = false;
  }

  return returnValue;
}

function setError(id, error) {
  let element = document.getElementById(id);
  element.getElementsByClassName("error-message")[0].innerHTML = error;
  // console.log(element);
}

function clearError() {
  error = document.getElementsByClassName("error-message");
  for (item in error) {
    item.innerHTML = "";
  }
}
